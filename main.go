package main

import (
	"log"

	"gitlab.com/polovenko.nikita/homefoods-backend-draft/api"
	"gitlab.com/polovenko.nikita/homefoods-backend-draft/config"

	// mysql is driver
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	authData "gitlab.com/polovenko.nikita/homefoods-backend-draft/authentication/data"
	authHandler "gitlab.com/polovenko.nikita/homefoods-backend-draft/authentication/httphandler"
	auth "gitlab.com/polovenko.nikita/homefoods-backend-draft/authentication/service"
	homefoodsData "gitlab.com/polovenko.nikita/homefoods-backend-draft/homefoods/data"
	homefdsHandler "gitlab.com/polovenko.nikita/homefoods-backend-draft/homefoods/httphandler"
	homefoods "gitlab.com/polovenko.nikita/homefoods-backend-draft/homefoods/service"
)

var db *gorm.DB

func init() {
	config.Load()
}

func main() {

	db, err := gorm.Open(config.DBDRIVER, config.DBURL)
	if err != nil {
		log.Fatal(err)
	}

	defer db.Close()

	authData.Migrate(db)
	homefoodsData.Migrate(db)

	authDBService := authData.NewService(db)
	authService, err := auth.NewService(authDBService)
	if err != nil {
		log.Fatal(err)
	}
	authHandler, err := authHandler.NewHandler(authService)
	if err != nil {
		log.Fatal(err)
	}

	homefdsDBService := homefoodsData.NewService(db)
	homefdsSvcDeps := homefoods.Dependencies{
		AuthService: authService,
	}
	homefdsService, err := homefoods.NewService(homefdsDBService, homefdsSvcDeps)
	if err != nil {
		log.Fatal(err)
	}
	homefdsHandler, err := homefdsHandler.NewHandler(homefdsService)

	api.Run(authHandler, homefdsHandler)

}
