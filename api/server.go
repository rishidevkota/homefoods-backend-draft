package api

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	authHandler "gitlab.com/polovenko.nikita/homefoods-backend-draft/authentication/httphandler"
	"gitlab.com/polovenko.nikita/homefoods-backend-draft/config"
	homefdsHandler "gitlab.com/polovenko.nikita/homefoods-backend-draft/homefoods/httphandler"
)

func Run(autHandler *authHandler.Handler, homefdsHandler *homefdsHandler.Handler) {

	fmt.Printf("\n\tListening [::]:%d", config.PORT)
	gin.SetMode(config.GIN_MODE)
	router := gin.Default()

	router.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})

	user := router.Group("/api/v1/user")
	{
		user.POST("/login", autHandler.Login)
		user.POST("/register", autHandler.SignUp)
		user.GET("/", autHandler.AuthMiddleware(), autHandler.GetUserInfo)
		user.PUT("/", autHandler.AuthMiddleware(), autHandler.UpdateUserInfo)
	}

	dish := router.Group("/api/v1/dish")
	{
		dish.POST("/", autHandler.AuthMiddleware(), homefdsHandler.AddDish)
		dish.GET("/", autHandler.AuthMiddleware(), homefdsHandler.GetDishes)
		dish.PUT("/", autHandler.AuthMiddleware(), homefdsHandler.UpdateDish)
	}

	order := router.Group("/api/v1/order")

	{
		order.POST("/", autHandler.AuthMiddleware(), homefdsHandler.NewOrder)
		order.GET("/", autHandler.AuthMiddleware(), homefdsHandler.GetOrders)
		order.PUT("/", autHandler.AuthMiddleware(), homefdsHandler.CompleteOrder)
	}

	router.Run(fmt.Sprintf(":%d", config.PORT))
}
