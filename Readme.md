To run the application clone this repository with 
git clone https://gitlab.com/polovenko.nikita/homefoods-backend-draft.git

cd homefoods-backend-draft
and then set environment variables in file <docker-compose.yaml>
Once you are done with environment variables to start the application use command. 


docker-compose up -d --build

Now go to localhost:8081/api/v1/order <for verification> 

to stop the applicationa and delete containers use command. 

docker-compose down. 
