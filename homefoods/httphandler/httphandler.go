package http

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gitlab.com/polovenko.nikita/homefoods-backend-draft/homefoods"
)

type Handler struct {
	service homefoods.Service
}

func NewHandler(service homefoods.Service) (*Handler, error) {
	return &Handler{
		service: service,
	}, nil
}

func (h *Handler) AddDish(c *gin.Context) {
	dish := homefoods.Dish{}
	err := c.BindJSON(&dish)
	if err != nil {
		log.Error("Bind JSON error", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	err = h.service.AddDish(dish)
	if err != nil {
		log.Error("Error adding new dish", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	c.Status(http.StatusOK)
}

func (h *Handler) UpdateDish(c *gin.Context) {
	dish := homefoods.Dish{}
	err := c.BindJSON(&dish)
	if err != nil {
		log.Error("Bind JSON error", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	email := c.MustGet("email").(string)
	username := c.MustGet("username").(string)

	params := homefoods.UpdateDishParams{Email: email, Username: username}

	err = h.service.UpdateDish(dish, params)
	if err != nil {
		log.Error("Error updating the dish", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	c.Status(http.StatusOK)
}

func (h *Handler) GetDishes(c *gin.Context) {
	email := c.MustGet("email").(string)
	username := c.MustGet("username").(string)

	params := homefoods.GetDishesForUserParams{Email: email, Username: username}

	dishes, err := h.service.GetDishesForUser(params)
	if err != nil {
		log.Error("Error getting dishes", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	c.JSON(http.StatusOK, dishes)
}

func (h *Handler) NewOrder(c *gin.Context) {
	order := homefoods.Order{}
	err := c.BindJSON(&order)
	if err != nil {
		log.Error("Bind JSON error", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	err = h.service.NewOrder(order)
	if err != nil {
		log.Error("Error creating new order", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	c.Status(http.StatusOK)
}

func (h *Handler) GetOrders(c *gin.Context) {
	email := c.MustGet("email").(string)
	username := c.MustGet("username").(string)

	s := c.Query("uncompleted_only")
	showCompletedOnly, _ := strconv.ParseBool(s)

	params := homefoods.GetOrdersForUserParams{Email: email, Username: username, UncompletedOnly: &showCompletedOnly}

	dishes, err := h.service.GetOrders(params)
	if err != nil {
		log.Error("Error getting orders", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	c.JSON(http.StatusOK, dishes)
}

func (h *Handler) CompleteOrder(c *gin.Context) {
	completeRequest := homefoods.OrderCompleteRequest{}
	err := c.BindJSON(&completeRequest)
	if err != nil {
		log.Error("Bind JSON error", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	if completeRequest.OrderID == "" {
		log.Error("Invalid order ID", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	email := c.MustGet("email").(string)
	username := c.MustGet("username").(string)

	err = h.service.CompleteOrder(completeRequest, username, email)
	if err != nil {
		log.Error("Error completing the order", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	c.Status(http.StatusOK)
}

func (h *Handler) GetNotifications(c *gin.Context) {
	email := c.MustGet("email").(string)
	username := c.MustGet("username").(string)

	s := c.Query("uncompleted_only")
	showCompletedOnly, _ := strconv.ParseBool(s)

	params := homefoods.GetOrdersForUserParams{Email: email, Username: username, UncompletedOnly: &showCompletedOnly}

	dishes, err := h.service.GetOrders(params)
	if err != nil {
		log.Error("Error getting orders", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	c.JSON(http.StatusOK, dishes)
}
