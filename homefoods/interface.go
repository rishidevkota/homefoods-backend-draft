package homefoods

import (
	auth "gitlab.com/polovenko.nikita/homefoods-backend-draft/authentication"
)

type DBService interface {
	CreateDish(dish Dish) error
	GetDishesForUser(params GetDishesForUserParams) ([]Dish, error)
	UpdateDish(user auth.User, dish Dish) error
	NewOrder(order Order) error
	GetOrders(params GetOrdersForUserParams) ([]OrderReturnWrapper, error)
	CompleteOrder(orderID string) error
}

type Service interface {
	AddDish(dish Dish) error
	GetDishesForUser(params GetDishesForUserParams) ([]Dish, error)
	UpdateDish(dish Dish, params UpdateDishParams) error
	NewOrder(order Order) error
	GetOrders(params GetOrdersForUserParams) ([]OrderReturnWrapper, error)
	CompleteOrder(request OrderCompleteRequest, username string, email string) error
}

type AuthService interface {
	UserExists(user auth.User) (bool, error, auth.User)
}
