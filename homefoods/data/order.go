package data

import (
	"time"

	"gitlab.com/polovenko.nikita/homefoods-backend-draft/homefoods"
)

type Order struct {
	ID           uint64    `gorm:"primary_id;auto_increment"`
	CreatedAt    time.Time `gorm:"default:current_timestamp()"`
	UpdatedAt    time.Time `gorm:"default:current_timestamp()"`
	Completed    bool
	PriceInCents int
	CustomerID   uint64
	ChiefID      uint64
	DishID       int
	Allergies    string `gorm:"type:text"`
	Notes        string `gorm:"type:text"`
}

func (db *Service) NewOrder(order homefoods.Order) error {

	dbOrder := Order{
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
		Completed:    false,
		PriceInCents: order.PriceInCents,
		CustomerID:   order.CustomerID,
		ChiefID:      order.ChiefID,
		DishID:       order.DishID,
		Allergies:    order.Allergies,
		Notes:        order.Notes,
	}

	err := db.db.Create(&dbOrder).Error
	if err != nil {
		return err
	}

	return nil
}

func (db *Service) GetOrders(params homefoods.GetOrdersForUserParams) ([]homefoods.OrderReturnWrapper, error) {
	var orders []homefoods.OrderReturnWrapper
	var completed bool

	if params.UncompletedOnly == nil {
		completed = false
	} else {
		completed = !*params.UncompletedOnly
	}

	err := db.db.Raw(`SELECT
	o.*, d.name, d.description, d.ingredients, d.eta_in_minutes, u.username, u.phone, u.city, u.zip 
	FROM orders o 
	INNER JOIN dishes d on o.dish_id = d.id 
	INNER JOIN users u on o.customer_id = u.id
	WHERE o.completed = ?
	 `, completed).Scan(&orders).Error

	if err != nil {
		return orders, err
	}

	return orders, nil
}

func (db *Service) CompleteOrder(orderID string) error {
	var o Order

	err := db.db.Model(&o).Where("id = ?", orderID).Update("completed", true).Error
	if err != nil {
		return err
	}

	return nil
}
