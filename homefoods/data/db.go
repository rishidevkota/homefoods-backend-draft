package data

import (
	"github.com/jinzhu/gorm"
	log "github.com/sirupsen/logrus"
)

type Service struct {
	db *gorm.DB
}

func NewService(db *gorm.DB) *Service {
	if db == nil {
		log.Info("Initialized homefoods.dbService with empty database pointer")
	}
	return &Service{db}
}
