package data

import (
	"log"

	"github.com/jinzhu/gorm"
)

func Migrate(db *gorm.DB) error {

	err := db.AutoMigrate(&Dish{}).Error
	if err != nil {
		log.Fatal(err)
	}

	err = db.AutoMigrate(&Order{}).Error
	if err != nil {
		log.Fatal(err)
	}

	return nil
}
