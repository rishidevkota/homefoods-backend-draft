package data

import (
	"time"

	"gitlab.com/polovenko.nikita/homefoods-backend-draft/authentication"
	"gitlab.com/polovenko.nikita/homefoods-backend-draft/homefoods"
)

type Dish struct {
	ID           uint64    `gorm:"primary_id;auto_increment"`
	CreatedAt    time.Time `gorm:"default:current_timestamp()"`
	UpdatedAt    time.Time `gorm:"default:current_timestamp()"`
	Name         string
	Description  string `gorm:"type:text"`
	Ingredients  string `gorm:"type:text"`
	EtaInMinutes int
	PriceInCents int
	Picture      string
	UserID       uint64
	IsActive     bool
}

func (db *Service) CreateDish(dish homefoods.Dish) error {

	dbDish := Dish{
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
		Name:         dish.Name,
		Description:  dish.Description,
		Ingredients:  dish.Ingredients,
		EtaInMinutes: dish.EtaInMinutes,
		PriceInCents: dish.PriceInCents,
		Picture:      dish.Picture,
		UserID:       dish.UserID,
		IsActive:     true,
	}

	err := db.db.Create(&dbDish).Error
	if err != nil {
		return err
	}

	return nil
}

func (db *Service) GetDishesForUser(params homefoods.GetDishesForUserParams) ([]homefoods.Dish, error) {
	var dishes []Dish
	var err error

	if !params.ShowActiveOnly {
		err = db.db.Where("user_id = ?", params.UserID).Find(&dishes).Error
	} else {
		err = db.db.Where("user_id = ? AND is_active = true", params.UserID).Find(&dishes).Error
	}

	if err != nil {
		return []homefoods.Dish{}, err
	}

	var homefoodsDishes []homefoods.Dish
	for _, dish := range dishes {
		homefdsdish := homefoods.Dish{
			ID:           dish.ID,
			Name:         dish.Name,
			Description:  dish.Description,
			Ingredients:  dish.Ingredients,
			EtaInMinutes: dish.EtaInMinutes,
			PriceInCents: dish.PriceInCents,
			Picture:      dish.Picture,
			UserID:       dish.UserID,
			IsActive:     dish.IsActive,
		}
		homefoodsDishes = append(homefoodsDishes, homefdsdish)
	}

	return homefoodsDishes, nil
}

func (db *Service) UpdateDish(user authentication.User, dish homefoods.Dish) error {

	var dbDish Dish

	query := db.db.Model(&dbDish).Where(`id = ?`, dish.ID).Updates(map[string]interface{}{
		"name":         dish.Name,
		"description":  dish.Description,
		"ingredients":  dish.Ingredients,
		"etaInMinutes": dish.EtaInMinutes,
		"priceInCents": dish.PriceInCents,
		"pictures":     dish.Picture,
		"is_active":    dish.IsActive,
		"updated_at":   time.Now().UTC(),
	})

	err := query.Error

	if query.Error != nil {
		return err
	}

	return nil
}
