package service

import (
	"fmt"

	"gitlab.com/polovenko.nikita/homefoods-backend-draft/authentication"
	"gitlab.com/polovenko.nikita/homefoods-backend-draft/homefoods"
)

type Dependencies struct {
	AuthService homefoods.AuthService
}

type Service struct {
	DBService homefoods.DBService
	*Dependencies
}

const Chief string = "Chief"
const Customer string = "Customer"

func NewService(DBService homefoods.DBService, dependencies Dependencies) (*Service, error) {
	if dependencies.AuthService == nil {
		return nil, fmt.Errorf("AuthService expects non nil")
	}

	return &Service{
		DBService:    DBService,
		Dependencies: &dependencies,
	}, nil
}

func (s *Service) AddDish(dish homefoods.Dish) error {
	return s.DBService.CreateDish(dish)
}

func (s *Service) GetDishesForUser(params homefoods.GetDishesForUserParams) ([]homefoods.Dish, error) {
	_, err, user := s.AuthService.UserExists(authentication.User{Email: params.Email, Username: params.Username})
	if err != nil {
		return []homefoods.Dish{}, err
	}

	params = homefoods.GetDishesForUserParams{
		UserID:         user.ID,
		ShowActiveOnly: true,
	}

	dishes, err := s.DBService.GetDishesForUser(params)
	if err != nil {
		return []homefoods.Dish{}, err
	}

	return dishes, nil
}

func (s *Service) UpdateDish(dish homefoods.Dish, params homefoods.UpdateDishParams) error {
	_, err, user := s.AuthService.UserExists(authentication.User{Email: params.Email, Username: params.Username})
	if err != nil {
		return err
	}

	if user.ID != dish.UserID {
		return fmt.Errorf("Dish does not belong to the auth user")
	}

	return s.DBService.UpdateDish(user, dish)
}

func (s *Service) NewOrder(order homefoods.Order) error {
	return s.DBService.NewOrder(order)
}

func (s *Service) GetOrders(params homefoods.GetOrdersForUserParams) ([]homefoods.OrderReturnWrapper, error) {
	_, err, user := s.AuthService.UserExists(authentication.User{Email: params.Email, Username: params.Username})
	if err != nil {
		return []homefoods.OrderReturnWrapper{}, err
	}

	if user.Type == Chief {
		params.IsChief = true
	} else {
		params.IsChief = false
	}

	uncompletedOrdersOnly := true

	params = homefoods.GetOrdersForUserParams{
		UserID:          user.ID,
		UncompletedOnly: &uncompletedOrdersOnly,
		IsChief:         params.IsChief,
	}

	orders, err := s.DBService.GetOrders(params)
	if err != nil {
		return []homefoods.OrderReturnWrapper{}, err
	}

	return orders, nil
}

func (s *Service) CompleteOrder(request homefoods.OrderCompleteRequest, username string, email string) error {
	_, err, _ := s.AuthService.UserExists(authentication.User{Email: username, Username: email})
	if err != nil {
		return err
	}

	s.DBService.CompleteOrder(request.OrderID)
	return nil
}
