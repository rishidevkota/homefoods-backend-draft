package homefoods

type Dish struct {
	ID           uint64 `json:"id"`
	Name         string `json:"name"`
	Description  string `json:"description"`
	Ingredients  string `json:"ingredients"`
	EtaInMinutes int    `json:"eta_in_minutes"`
	PriceInCents int    `json:"price_in_cents"`
	Picture      string `json:"picture"`
	UserID       uint64 `json:"user_id"`
	IsActive     bool   `json:"is_active"`
}

type GetDishesForUserParams struct {
	UserID         uint64
	ShowActiveOnly bool
	Username       string
	Email          string
}

type UpdateDishParams struct {
	Username string
	Email    string
}

type Order struct {
	ID           uint64 `json:"id"`
	Completed    bool   `json:"completed"`
	PriceInCents int    `json:"price_in_cents"`
	CustomerID   uint64 `json:"customer_id"`
	ChiefID      uint64 `json:"chief_id"`
	DishID       int    `json:"dish_id"`
	Allergies    string `json:"allergies"`
	Notes        string `json:"notes"`
}

type OrderReturnWrapper struct {
	ID           uint64 `json:"id"`
	Completed    bool   `json:"completed"`
	PriceInCents int    `json:"price_in_cents"`
	CustomerID   uint64 `json:"customer_id"`
	ChiefID      uint64 `json:"chief_id"`
	DishID       int    `json:"dish_id"`
	Allergies    string `json:"allergies"`
	Notes        string `json:"notes"`

	Name         string `json:"name"`
	Description  string `json:"description"`
	Ingredients  string `json:"ingredients"`
	EtaInMinutes int    `json:"eta_in_minutes"`

	Username string `json:"username"`
	Phone    string `json:"phone"`
	City     string `json:"city"`
	Zip      string `json:"zip"`
}

type GetOrdersForUserParams struct {
	UserID          uint64
	UncompletedOnly *bool
	Username        string
	Email           string
	IsChief         bool
}

type OrderCompleteRequest struct {
	OrderID string `json:"order_id"`
}
