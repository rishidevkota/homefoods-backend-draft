package service

import (
	"html"
	"reflect"
	"strings"

	"github.com/badoux/checkmail"
	"github.com/juju/errors"
	auth "gitlab.com/polovenko.nikita/homefoods-backend-draft/authentication"
)

func (s *Service) ValidateUser(u *auth.User) error {
	s.prepare(u)
	return s.validate("login", u)
}

// Prepare cleans the inputs
func (s *Service) prepare(u *auth.User) {
	u.Username = html.EscapeString(strings.TrimSpace(u.Username))
	u.Email = html.EscapeString(strings.TrimSpace(u.Email))
}

// Validate validates the inputs
func (s *Service) validate(action string, u *auth.User) error {
	switch strings.ToLower(action) {
	case "update":
		if u.Username == "" {
			return errors.New("Username is required")
		}

		if u.Email == "" {
			return errors.New("Email is required")
		}

		if err := checkmail.ValidateFormat(u.Email); err != nil {
			return errors.New("Invalid email")
		}
	case "login":
		if u.Email == "" {
			if u.Username == "" {
				return errors.New("Email or Username is required")
			}
		}

		if u.Password == "" {
			return errors.New("Password is required")
		}
	default:
		if u.Username == "" {
			return errors.New("Username is required")
		}

		if u.Password == "" {
			return errors.New("Password is required")
		}

		if u.Email == "" {
			return errors.New("Email is required")
		}

		if err := checkmail.ValidateFormat(u.Email); err != nil {
			return errors.New("Invalid email")
		}
	}

	return nil
}

// UserExists checks if the user already exists during registration
func (s *Service) UserExists(user auth.User) (bool, error, auth.User) {

	user.Email = strings.ToLower(user.Email)
	user.Username = strings.ToLower(user.Username)

	if user.Email == "" {
		return true, errors.New("Email is empty"), user
	}

	userCheck, err := s.DBService.GetUserByEmail(user.Email)
	if err != nil {
		if err.Error() != "record not found" {
			return true, err, auth.User{}
		}
	}

	if user.Username == "" {
		return true, errors.New("Username is empty"), user
	}

	userCheck2, err := s.DBService.GetUserByUsername(user.Username)
	if err != nil {
		if err.Error() != "record not found" {
			return true, err, auth.User{}
		}
	}

	return !reflect.DeepEqual(userCheck, auth.User{}) || !reflect.DeepEqual(userCheck2, auth.User{}), nil, userCheck2
}

func (s *Service) UpdateUser(user auth.User) error {
	return s.DBService.UpdateUser(user)
}
