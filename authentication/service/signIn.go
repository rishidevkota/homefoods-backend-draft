package service

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/polovenko.nikita/homefoods-backend-draft/authentication"
)

// Login method
func (s *Service) Login(email, username string, password string) (string, error) {
	var user authentication.User
	var err error

	if username != "" {
		user, err = s.DBService.GetUserByUsername(username)
		if err != nil {
			log.Error("Get user by username error", err)
			return "", nil
		}
	}

	if email != "" {
		user, err = s.DBService.GetUserByEmail(email)
		if err.Error() != "record not found" {
			log.Error("Get user by email error", err)
			return "", nil
		}
	}

	err = s.VerifyPassword(user.Password, password)
	if err != nil {
		log.Error("Verify Password Error ", err)
		return "", nil
	}

	user.Password = ""
	return s.GenerateJWT(user)
}
