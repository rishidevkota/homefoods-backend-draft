package service

import (
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/juju/errors"
	auth "gitlab.com/polovenko.nikita/homefoods-backend-draft/authentication"
	"gitlab.com/polovenko.nikita/homefoods-backend-draft/config"
)

// GenerateJWT creates a new token to the client
func (s *Service) GenerateJWT(user auth.User) (string, error) {
	claim := auth.Claim{
		User: user,
		StandardClaims: jwt.StandardClaims{
			Issuer:    "Nik Polovenko",
			ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	return token.SignedString(config.SECRETKEY)
}

// ParseJWTToken ...
func (s *Service) ParseJWTToken(tokenString string) (*jwt.Token, error) {
	claim := &auth.Claim{}

	token, err := jwt.ParseWithClaims(tokenString, claim, func(_ *jwt.Token) (interface{}, error) {
		return config.SECRETKEY, nil
	})

	if err != nil {
		switch err.(type) {
		case *jwt.ValidationError:
			vError := err.(*jwt.ValidationError)
			switch vError.Errors {
			case jwt.ValidationErrorExpired:
				err = errors.New("Your token has expired")
				return nil, err
			case jwt.ValidationErrorSignatureInvalid:
				err = errors.New("The signature is invalid")
				return nil, err
			default:
				return nil, err
			}
		}
	}
	if !token.Valid {
		errors.NewUnauthorized(err, "invalid jwt token")
		return nil, err
	}
	return token, nil
}

// RefreshJWTToken ...
func (s *Service) RefreshJWTToken(user auth.User, tokenString string) (string, error) {
	_, err := s.ParseJWTToken(tokenString)
	if err != nil {
		return tokenString, err
	}
	tokenString, err = s.GenerateJWT(user)
	if err != nil {
		return tokenString, err
	}
	return tokenString, nil
}
