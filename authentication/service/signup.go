package service

import (
	"fmt"
	"regexp"

	"gitlab.com/polovenko.nikita/homefoods-backend-draft/authentication"
)

func (s *Service) Register(user authentication.User) (string, error) {
	if len(user.Password) < 4 {
		return "", fmt.Errorf("Password must be atleast 4 characters long")
	}

	if len(user.Email) < 4 {
		return "", fmt.Errorf("Email must be atleast 4 characters long")
	}

	if !s.isEmailValid(user.Email) {
		return "", fmt.Errorf("Email is invalid")
	}

	if len(user.Username) < 4 {
		return "", fmt.Errorf("Username must be atleast 4 characters long")
	}

	err := s.DBService.CreateUser(user)
	if err != nil {
		return "", err
	}

	return s.GenerateJWT(user)
}

// isEmailValid checks if the email provided passes the required structure and length.
func (s *Service) isEmailValid(e string) bool {

	var emailRegex = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

	if len(e) < 3 && len(e) > 254 {
		return false
	}
	return emailRegex.MatchString(e)
}
