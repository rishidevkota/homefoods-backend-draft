package service

import "gitlab.com/polovenko.nikita/homefoods-backend-draft/authentication"

type Service struct {
	DBService authentication.DBService
}

func NewService(DBService authentication.DBService) (*Service, error) {
	return &Service{
		DBService: DBService,
	}, nil
}
