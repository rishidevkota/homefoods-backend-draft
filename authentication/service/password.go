package service

import (
	"golang.org/x/crypto/bcrypt"
)

// VerifyPassword verify the hashed password
func (s *Service) VerifyPassword(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}
