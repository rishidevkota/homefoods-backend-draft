package http

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	auth "gitlab.com/polovenko.nikita/homefoods-backend-draft/authentication"
)

func (h *Handler) AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		//const BEARER_SCHEMA = "Bearer"
		authHeader := c.GetHeader("Authorization")
		if !strings.HasPrefix(authHeader, "Bearer ") {
			c.Status(http.StatusBadRequest)
			log.Info("Missing token or bearer prefix")
			c.Abort()
			return
		}
		bearer := strings.TrimPrefix(authHeader, "Bearer ")

		//tokenString := authHeader[len(BEARER_SCHEMA):]
		token, err := h.service.ParseJWTToken(bearer)
		if token == nil {
			log.Info("Invalid token. Error is: ", err)
			c.Status(http.StatusBadRequest)
			c.Abort()
			return
		}

		if token.Valid {
			claims := token.Claims.(*auth.Claim).User
			log.Info("claims: ", claims)
			c.Set("username", claims.Username)
			c.Set("email", claims.Email)
		} else {
			c.AbortWithStatus(http.StatusUnauthorized)
			log.Info(err)
			return
		}
	}
}
