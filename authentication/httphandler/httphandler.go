package http

import (
	"net/http"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gitlab.com/polovenko.nikita/homefoods-backend-draft/authentication"
	auth "gitlab.com/polovenko.nikita/homefoods-backend-draft/authentication"
)

type Handler struct {
	service auth.Service
}

func NewHandler(service auth.Service) (*Handler, error) {
	return &Handler{
		service: service,
	}, nil
}

func (h *Handler) Login(c *gin.Context) {
	user := auth.User{}

	err := c.BindJSON(&user)
	if err != nil {
		log.Error("Bind JSON error", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	err = h.service.ValidateUser(&user)

	if err != nil {
		log.Error("ValidateUser error", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	token, err := h.service.Login(user.Email, user.Username, user.Password)
	if err != nil {
		log.Error("Login failed", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	c.JSON(http.StatusOK, token)
}

//SignUp is the registration function
func (h *Handler) SignUp(c *gin.Context) {
	user := auth.User{}

	err := c.BindJSON(&user)
	if err != nil {
		log.Error("Bind JSON error", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	userExists, err, _ := h.service.UserExists(user)

	if err != nil {
		log.Error("ValidateUser error", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	if userExists {
		c.JSON(http.StatusOK, gin.H{"error": "user already exists"})
		c.Abort()
		return
	}

	token, err := h.service.Register(user)

	if err != nil {
		log.Error("SignUp failed", err)
		c.JSON(http.StatusBadRequest, gin.H{"error:": err.Error()})
		c.Abort()
		return
	}

	c.JSON(http.StatusOK, token)
}

// GetUserInfo is used to populate user profile
func (h *Handler) GetUserInfo(c *gin.Context) {

	username := c.MustGet("username").(string)

	email := c.MustGet("email").(string)

	user := authentication.User{
		Username: username,
		Email:    email,
	}

	userExists, err, user := h.service.UserExists(user)

	if err != nil {
		log.Error("ValidateUser error", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	if !userExists {
		c.JSON(http.StatusOK, gin.H{"error": "user doesn't exist"})
		c.Abort()
		return
	}

	user.Password = ""

	c.JSON(http.StatusOK, user)
}

func (h *Handler) UpdateUserInfo(c *gin.Context) {
	user := auth.User{}

	err := c.BindJSON(&user)

	if err != nil {
		log.Error("Bind JSON error", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	username := c.MustGet("username").(string)

	email := c.MustGet("email").(string)

	if username != user.Username || email != user.Email {
		log.Error("User in the request body is not the auth user")
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	userExists, err, _ := h.service.UserExists(user)

	if err != nil {
		log.Error("ValidateUser error", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	if !userExists {
		c.JSON(http.StatusOK, gin.H{"error": "user doesn't exist"})
		c.Abort()
		return
	}

	user.Password = ""

	err = h.service.UpdateUser(user)

	if err != nil {
		log.Error("Update user error", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	c.Status(http.StatusOK)
}
