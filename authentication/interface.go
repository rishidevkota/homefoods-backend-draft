package authentication

import "github.com/dgrijalva/jwt-go"

type DBService interface {
	GetUserByUsername(username string) (User, error)
	GetUserByEmail(email string) (User, error)
	CreateUser(user User) error
	UpdateUser(user User) error
}

type Service interface {
	ValidateUser(u *User) error
	Login(email, username, password string) (string, error)
	ParseJWTToken(tokenString string) (*jwt.Token, error)
	UserExists(user User) (bool, error, User)
	Register(user User) (string, error)
	UpdateUser(user User) error
}
