package data

import (
	"log"

	"github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

// Load autogenerates the tables and records
func Migrate(db *gorm.DB) error {

	err := db.AutoMigrate(&User{}).Error
	if err != nil {
		log.Fatal(err)
	}

	for _, user := range testUsers {

		hashedPassword, err := Hash(user.Password)
		if err != nil {
			return err
		}
		user.Password = string(hashedPassword)

		err = db.Model(&User{}).Create(&user).Error
		if err != nil {
			switch e := err.(type) {
			case *mysql.MySQLError:
				if e.Number == 1062 {
					log.Print("Ignoring duplicate user entry")
					return nil
				}
				return err
			default:
				return err
			}
		}

	}

	return nil
}

// Hash make a password hash
func Hash(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}
