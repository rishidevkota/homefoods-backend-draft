package data

import (
	"time"

	"gitlab.com/polovenko.nikita/homefoods-backend-draft/authentication"
)

const userIsChief int = 2
const userIsCustomer int = 1

// User model
type User struct {
	ID               uint64    `gorm:"primary_id;auto_increment" json:"id"`
	CreatedAt        time.Time `gorm:"default:current_timestamp()" json:"created_at"`
	UpdatedAt        time.Time `gorm:"default:current_timestamp()" json:"updated_at"`
	Username         string    `gorm:"size:20;not null;unique" json:"username"`
	Email            string    `gorm:"size:100;not null;unique" json:"email"`
	Password         string    `gorm:"size:60;not null" json:"password,omitempty"`
	Avatar           string
	Phone            string
	City             string
	Zip              string
	Delivers         bool
	DeliveryRange    int
	Type             string
	About            string `gorm:"type:text"`
	Info             string `gorm:"type:text"`
	AdditionalDetail string `gorm:"type:text"`
}

var testUsers = []User{}

func (db *Service) GetUserByEmail(email string) (authentication.User, error) {
	user := User{}
	var err error

	err = db.db.Model(User{}).Where("email = ?", email).Take(&user).Error
	if err != nil {
		return authentication.User{}, err
	}

	returnUser := authentication.User{
		Username:         user.Username,
		Email:            user.Email,
		Password:         user.Password,
		Avatar:           user.Avatar,
		Phone:            user.Phone,
		City:             user.City,
		Zip:              user.Zip,
		Delivers:         user.Delivers,
		DeliveryRange:    user.DeliveryRange,
		Type:             user.Type,
		About:            user.About,
		Info:             user.Info,
		AdditionalDetail: user.AdditionalDetail,
	}

	return returnUser, nil
}

func (db *Service) GetUserByUsername(username string) (authentication.User, error) {
	user := User{}
	var err error

	err = db.db.Model(User{}).Where("username = ?", username).Take(&user).Error
	if err != nil {
		return authentication.User{}, err
	}

	returnUser := authentication.User{
		ID:               user.ID,
		Username:         user.Username,
		Email:            user.Email,
		Password:         user.Password,
		Avatar:           user.Avatar,
		Phone:            user.Phone,
		City:             user.City,
		Zip:              user.Zip,
		Delivers:         user.Delivers,
		DeliveryRange:    user.DeliveryRange,
		Type:             user.Type,
		About:            user.About,
		Info:             user.Info,
		AdditionalDetail: user.AdditionalDetail,
	}

	return returnUser, nil
}

func (db *Service) CreateUser(user authentication.User) error {
	hashedPassword, err := Hash(user.Password)
	if err != nil {
		return err
	}
	user.Password = string(hashedPassword)

	dbUser := User{
		CreatedAt:        time.Now(),
		UpdatedAt:        time.Now(),
		Username:         user.Username,
		Email:            user.Email,
		Password:         user.Password,
		Avatar:           user.Avatar,
		Phone:            user.Phone,
		City:             user.City,
		Zip:              user.Zip,
		Delivers:         user.Delivers,
		DeliveryRange:    user.DeliveryRange,
		Type:             user.Type,
		About:            user.About,
		Info:             user.Info,
		AdditionalDetail: user.AdditionalDetail,
	}

	err = db.db.Model(&User{}).Create(&dbUser).Error
	if err != nil {
		return err
	}

	return nil
}

func (db *Service) UpdateUser(user authentication.User) error {

	var dbUser User

	query := db.db.Model(&dbUser).Where(`id = ?`, user.ID).Updates(map[string]interface{}{
		"email":             user.Email,
		"password":          user.Password,
		"avatar":            user.Avatar,
		"phone":             user.Phone,
		"city":              user.City,
		"zip":               user.Zip,
		"delivers":          user.Delivers,
		"delivery_range":    user.DeliveryRange,
		"type":              user.Type,
		"about":             user.About,
		"info":              user.Info,
		"additional_detail": user.AdditionalDetail,
		"updated_at":        time.Now().UTC(),
	})

	err := query.Error

	if query.Error != nil {
		return err
	}

	return nil
}
