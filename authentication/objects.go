package authentication

import "github.com/dgrijalva/jwt-go"

// User model
type User struct {
	ID               uint64 `json:"id"`
	Username         string `json:"username"`
	Email            string `json:"email"`
	Password         string `json:"password"`
	Avatar           string `json:"avatar"`
	Phone            string `json:"phone"`
	City             string `json:"city"`
	Zip              string `json:"zip"`
	Delivers         bool   `json:"delivers"`
	DeliveryRange    int    `json:"delivery_range"`
	Type             string `json:"type"`
	About            string `json:"about"`
	Info             string `json:"info"`
	AdditionalDetail string `json:"additional_detail"`
}

// Claim is the token payload
type Claim struct {
	User User `json:"user"`
	jwt.StandardClaims
}
