module gitlab.com/polovenko.nikita/homefoods-backend-draft

go 1.15

require (
	github.com/badoux/checkmail v1.2.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.3.0
	github.com/juju/errors v0.0.0-20200330140219-3fe23663418f
	github.com/juju/testing v0.0.0-20210302031854-2c7ee8570c07 // indirect
	github.com/sirupsen/logrus v1.8.0
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
)
